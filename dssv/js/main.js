const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// Chức năng thêm sinh viên
var dssv = [];

// lấy thông tin từ localStorage

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    let sv = dssv[index];

    dssv[index] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.matKhau,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}

function themSV() {
  var newSV = getInfoFromForm();
  var isValid =
    validation.kiemTraRong(
      newSV.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.kiemTraSo(newSV.ma, "spanMaSV", "Mã sinh viên phải là số") &&
    validation.kiemTraDoDai(
      newSV.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );

  isValid =
    isValid &
      validation.kiemTraRong(
        newSV.ten,
        "spanTenSV",
        "Tên sinh viên không được rỗng"
      ) &&
    validation.kiemTraChu(newSV.ten, "spanTenSV", "Tên sinh viên phải là chữ");

  isValid =
    isValid &
      validation.kiemTraRong(
        newSV.email,
        "spanEmailSV",
        "Email sinh viên không được rỗng"
      ) &&
    validation.kiemTraEmail(
      newSV.email,
      "spanEmailSV",
      "Email sinh viên không hợp lệ"
    );

  isValid =
    isValid &
      validation.kiemTraRong(
        newSV.matKhau,
        "spanMatKhau",
        "Mật khẩu không được rỗng"
      ) &&
    validation.kiemTraMatKhau(
      newSV.matKhau,
      "spanMatKhau",
      "Mật khẩu phải có ít nhất 8 chữ số, gồm ít nhất 1 chữ và 1 số"
    );

  isValid =
    isValid &
      validation.kiemTraRong(
        newSV.toan,
        "spanToan",
        "Điểm Toán không được rỗng"
      ) &&
    validation.kiemTraGiơiHanSo(
      newSV.toan,
      "spanToan",
      "Điểm Toán phải là số lớn hơn 0 và nhỏ hơn 10"
    );

  isValid =
    isValid &
      validation.kiemTraRong(newSV.ly, "spanLy", "Điểm Lý không được rỗng") &&
    validation.kiemTraGiơiHanSo(
      newSV.ly,
      "spanLy",
      "Điểm Lý phải là số lớn hơn 0 và nhỏ hơn 10"
    );

  isValid =
    isValid &
      validation.kiemTraRong(
        newSV.hoa,
        "spanHoa",
        "Điểm Hóa không được rỗng"
      ) &&
    validation.kiemTraGiơiHanSo(
      newSV.hoa,
      "spanHoa",
      "Điểm Hóa phải là số lớn hơn 0 và nhỏ hơn 10"
    );

  if (isValid) {
    dssv.push(newSV);

    const uniqueIds = [];

    const unique = dssv.filter((element) => {
      const isDuplicate = uniqueIds.includes(element.ma);

      if (!isDuplicate) {
        uniqueIds.push(element.ma);

        return true;
      }

      return false;
    });

    // tạo json
    var dssvJson = JSON.stringify(unique);

    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(unique);
  }
}

function xoaSinhVien(id) {
  console.log("id: ", id);

  var index = timKiemViTri(id, dssv);
  console.log(index);

  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);

    var dssvJson = JSON.stringify(dssv);

    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  window["sua_id"] = id;
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    const signal = "sua";
    showInfoToForm(sv, signal);
  }
}

function resetInfoSV(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index == -1) {
    resetInfoInForm(window[`signal`]);
  }
}

function updateInfoSV() {
  const id = window["sua_id"];

  const dataUpdate = getInfoFromForm();

  const dataFilter = dssv.filter((f) => f.ma !== id);
  dataFilter.push(dataUpdate);
  console.log("dataFilter: ", dataFilter);

  //save localstorage

  const dataFilterJson = JSON.stringify(dataFilter);

  localStorage.setItem("DSSV_LOCALSTORAGE", dataFilterJson);

  renderDSSV(dataFilter);
}

function searchName(name) {
  let teninput = getNameFromSearch(name);

  let found = dssv.filter((f) => f.ten.includes(teninput));
  console.log("found: ", found);
  renderDSSV(found);
}
