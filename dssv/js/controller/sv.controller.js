function getInfoFromForm() {
  const maSv = document.getElementById(`txtMaSV`).value;
  const tenSv = document.getElementById(`txtTenSV`).value;
  const emailSv = document.getElementById(`txtEmail`).value;
  const passSv = document.getElementById(`txtPass`).value;
  const diemToan = document.getElementById(`txtDiemToan`).value;
  const diemLy = document.getElementById(`txtDiemLy`).value;
  const diemHoa = document.getElementById(`txtDiemHoa`).value;

  var sv = new SinhVien(
    maSv,
    tenSv,
    emailSv,
    passSv,
    diemToan,
    diemLy,
    diemHoa
  );

  return sv;
}

function getNameFromSearch(name) {
  return (name = document.getElementById(`txtSearch`).value);
}

var dssv = [];
function renderDSSV(svArr) {
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];

    if (
      sv.ma != "" &&
      sv.ten != "" &&
      sv.email != "" &&
      sv.matKhau != "" &&
      sv.toan != "" &&
      sv.ly != "" &&
      sv.hoa != ""
    ) {
      var trContent = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.ma
    }')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.ma
    }')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;

      contentHTML += trContent;
    } else {
      return false;
    }
  }

  document.getElementById(`tbodySinhVien`).innerHTML = contentHTML;
}

function renderSearch(svArr) {
  // window[`signal1`] = signal;
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];

    var trContent = ` <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.ma
    }')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.ma
    }')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;

    contentHTML += trContent;
  }

  document.getElementById(`tbodySinhVien`).innerHTML = contentHTML;
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }

  return -1;
}

function showInfoToForm(sv, signal) {
  window[`signal`] = signal;
  document.getElementById(`txtMaSV`).value = sv.ma;
  document.getElementById(`txtTenSV`).value = sv.ten;
  document.getElementById(`txtEmail`).value = sv.email;
  document.getElementById(`txtPass`).value = sv.matKhau;
  document.getElementById(`txtDiemToan`).value = sv.toan;
  document.getElementById(`txtDiemLy`).value = sv.ly;
  document.getElementById(`txtDiemHoa`).value = sv.hoa;
  document.getElementById(`txtMaSV`).readOnly = true;
}

function resetInfoInForm(noedit) {
  if (!noedit) {
    document.getElementById(`txtMaSV`).value = "";
    document.getElementById(`txtTenSV`).value = "";
    document.getElementById(`txtEmail`).value = "";
    document.getElementById(`txtPass`).value = "";
    document.getElementById(`txtDiemToan`).value = "";
    document.getElementById(`txtDiemLy`).value = "";
    document.getElementById(`txtDiemHoa`).value = "";
  } else {
    document.getElementById(`txtTenSV`).value = "";
    document.getElementById(`txtEmail`).value = "";
    document.getElementById(`txtPass`).value = "";
    document.getElementById(`txtDiemToan`).value = "";
    document.getElementById(`txtDiemLy`).value = "";
    document.getElementById(`txtDiemHoa`).value = "";
  }
}
